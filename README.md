# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (((λp.pz) (λq.w)) (λw.wqzp))

2. ((λp.pq) (λp.qp))


## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

For the following I just went from left to right for each variable. 

1. λs.s z λq.s' q
s is bounded to λs, z is free, q is bounded to λq and s' is free

2. (λs. s z) λq. w λw. w q z s
s is bounded to λs, z is free, q is free, w is free, w is bounded to λw, q z s are all free.

3. (λs.s) (λq.qs)
s is bounded to λs, q is bounded to λq and s is free.

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
s is bounded to λs, q is free, q is bounded to λq and z is free, both of the z is bounded to λz

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
    (z) (λs.s a) [z -> (λq.q q)]
    (λq.q q) (λs.s a)
    (q q) [q -> (λs.s a)]
    ((λs.s a) (λs'.s' a'))
    (s a) [s -> (λs'.s' a')]
    ((λs'.s' a') a)
    (s' a') [s' -> a]
    (a a')

2. (λz.z) (λz.z z) (λz.z q)
    (z) (λz''.z'' q) [z -> (λz.z' z')]
    (λz.z' z') (λz''.z'' q)
    (z' z') [z' -> (λz''.z'' q)]
    ((λz''.z'' q)(λz''.z'' q'))
    (z'' q) [z'' -> (λz''.z'' q')]
    ((λz''.z'' q') q)
    (z'' q') [z'' -> q]
    (q q')

3. (λs.λq.s q q) (λa.a) b
    (λq.s q q) b [s -> (λa.a)] 
    (λq. (λa.a) q q) b
    ((λa.a) q q)  [q -> b]
    (λa.a) b b
    (a) b [a -> b']
    b' b

4. (λs.λq.s q q) (λq.q) q
    (λs.λq.s q q) (λq'.q') q''
    (λq.s q q) q'' [s -> (λq'.q')]
    (λq. (λq'.q') q q) q''
    ( (λq'.q') q q) [q -> q'']
    ( (λq'.q') q'' q'')
    (q') q''  [q' -> q''']
    q''' q''
    
5. ((λs.s s) (λq.q)) (λq.q)
    (s s)  (λq.q) [s -> (λq'.q')]
    ((λq'.q') (λq'.q')) (λq.q)
    (q') (λq.q) [q' -> (λq'.q')]
    (λq'.q') (λq.q)
    (q') [q' -> (λq.q) ]
    (λq.q) 

## Question 4

1. Write the truth table for the or operator below.
x y x+y
0 0 0
1 0 1
0 1 1
1 1 1

2. The Church encoding for OR = (λp.λq.p p q)

T = λp.λq.p
F = λp.λq.q

FF -> F
(λp.λq.p p q) (λp.λq.q) (λp.λq.q) --> should return λp.λq.q
(λq.p p q) (λp.λq.q) [p -> (λp'.λq'.q')]
(λq.(λp'.λq'.q') (λp'.λq'.q') q) (λp.λq.q)
((λp'.λq'.q') (λp'.λq'.q') q) [q -> (λp''.λq''.q'')]
((λp'.λq'.q') (λp'.λq'.q') (λp''.λq''.q''))
since this only returns q, I will only replace q
(q') [q' -> (λp''.λq''.q'')]
(λp''.λq''.q'') --> F


TF -> T
(λp.λq.p p q) T F
(λq.p p q) F [p -> T]
(λq.T T q) F
(T T q) [q -> F] 
(T T F)
T
here T takes in T and F, and since T always returns the first one, so T is returned  

FT -> T
(λp.λq.p p q) F T
(F F T)
F returns the second argument 
T

TT -> T
(λp.λq.p p q) T T
(T T T)
T returns the first argument
T

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 


## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.
not = λp.λa.λb.pba
T = λp.λq.p
F = λp.λq.q

not true = (λp.λa.λb.pba) T
(λp.λa.λb.pba) T
(λa.λb.pba) [p -> T]
(λa.λb.Tba) 
(λa.λb.(λp.λq.p)ba) 
(λa.λb.b) = F 

It's basically like 
if (p == True ) { //when you do not true
    ba becomes the input in T and returns b 
    since ab returns b, which is the second argument so its False
}
else { //when you do not false
    if the first input is false 
    then ba goes in F and returns a 
    since ab returns a, which is the first argument so its True
}

## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.